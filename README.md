# Schedules
```
git clone https://gitlab.com/LACMTA/gtfs_lax.git ;
cd gtfs_lax ;
for i in */; do zip -r -j "${i%/*}.zip" "$i"; done

# Append to this README
sed '57,$d' -i README.md ;
for i in *.zip; do printf "## [$i](https://media.metro.net/GTFS/$i) \
	\n\`\`\`bash\n `unzip -l "$i"` \n\`\`\`\n" >> README.md; done
```

# get schedules from the Metro Cloud Alliance
## update the local git repo
```
cd /Users/dgoodwin/Downloads/GTFS_LAX ;
git pull
```

## get the GTFS.ZIP files from metrocloudalliance.com
```
cd ..
wget -mpck --user-agent="" -e robots=off --user lacmta --password shhhh https://secure.metrocloudalliance.com/gtfs/
```

## update the ZIP file names

```python
import csv
f = open('lookup.csv')
csv_f = csv.reader(f)

for row in csv_f:
  msg = "mv secure.metrocloudalliance.com/gtfs/gtfs_datasets/%s %s.zip ;" %(row[0],row[1])
  print(msg)
```

## unzip the renamed files
`ls *.zip|awk -F'.zip' '{print "unzip "$0" -d "$1}'|sh`

## merge the new schedules with the git repository
```
rsync -Cav secure.metrocloudalliance.com/gtfs/gtfs_datasets/* /Users/dgoodwin/Downloads/GTFS_LAX/
```

## commit the changes and push
`git commit`


## Who's here?


## [alhambra_community.zip](https://media.metro.net/GTFS/alhambra_community.zip)     
```bash
 Archive:  alhambra_community.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      144  04-06-2017 21:32   agency.txt
      165  04-06-2017 21:33   calendar.txt
      363  04-06-2017 21:33   routes.txt
     2563  04-06-2017 21:33   stops.txt
   182666  04-06-2017 21:33   stop_times.txt
     9134  04-06-2017 21:33   trips.txt
     5516  04-06-2017 21:33   shapes.txt
      388  04-06-2017 21:33   transfers.txt
       94  04-06-2017 21:33   transfers2.txt
---------                     -------
   201033                     9 files 
```
## [amtrak.zip](https://media.metro.net/GTFS/amtrak.zip)     
```bash
 Archive:  amtrak.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      116  04-06-2017 21:29   agency.txt
      201  04-06-2017 21:30   calendar.txt
      321  04-06-2017 21:30   routes.txt
     2028  04-06-2017 21:30   stops.txt
    31885  04-06-2017 21:30   stop_times.txt
     2381  04-06-2017 21:30   trips.txt
     5736  04-06-2017 21:30   shapes.txt
      220  04-06-2017 21:30   transfers.txt
       38  04-06-2017 21:30   transfers2.txt
---------                     -------
    42926                     9 files 
```
## [antelope_valley.zip](https://media.metro.net/GTFS/antelope_valley.zip)     
```bash
 Archive:  antelope_valley.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      141  04-06-2017 21:33   agency.txt
      308  04-06-2017 21:34   calendar.txt
     2430  04-06-2017 21:34   routes.txt
    29815  04-06-2017 21:34   stops.txt
  2109176  04-06-2017 21:34   stop_times.txt
    65460  04-06-2017 21:34   trips.txt
    62880  04-06-2017 21:34   shapes.txt
     2082  04-06-2017 21:34   transfers.txt
      486  04-06-2017 21:34   transfers2.txt
---------                     -------
  2272778                     9 files 
```
## [baldwin_park.zip](https://media.metro.net/GTFS/baldwin_park.zip)     
```bash
 Archive:  baldwin_park.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      134  04-06-2017 21:39   agency.txt
      229  04-06-2017 21:40   calendar.txt
      661  04-06-2017 21:40   routes.txt
     3707  04-06-2017 21:40   stops.txt
   469945  04-06-2017 21:40   stop_times.txt
     6893  04-06-2017 21:40   trips.txt
    30157  04-06-2017 21:40   shapes.txt
      346  04-06-2017 21:40   transfers.txt
       66  04-06-2017 21:40   transfers2.txt
---------                     -------
   512138                     9 files 
```
## [beach_cities.zip](https://media.metro.net/GTFS/beach_cities.zip)     
```bash
 Archive:  beach_cities.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      168  04-06-2017 21:34   agency.txt
      347  04-06-2017 21:36   calendar.txt
      263  04-06-2017 21:36   routes.txt
     8280  04-06-2017 21:36   stops.txt
   769879  04-06-2017 21:36   stop_times.txt
    16501  04-06-2017 21:36   trips.txt
    13015  04-06-2017 21:36   shapes.txt
      892  04-06-2017 21:36   transfers.txt
      206  04-06-2017 21:36   transfers2.txt
---------                     -------
   809551                     9 files 
```
## [bell_gardens.zip](https://media.metro.net/GTFS/bell_gardens.zip)     
```bash
 Archive:  bell_gardens.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      140  04-06-2017 21:37   agency.txt
      127  04-06-2017 21:38   calendar.txt
      149  04-06-2017 21:38   routes.txt
     3213  04-06-2017 21:38   stops.txt
   122398  04-06-2017 21:38   stop_times.txt
     2473  04-06-2017 21:38   trips.txt
     5412  04-06-2017 21:38   shapes.txt
      248  04-06-2017 21:38   transfers.txt
       38  04-06-2017 21:38   transfers2.txt
---------                     -------
   134198                     9 files 
```
## [bellflower_bus.zip](https://media.metro.net/GTFS/bellflower_bus.zip)     
```bash
 Archive:  bellflower_bus.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      127  04-06-2017 21:36   agency.txt
      126  04-06-2017 21:37   calendar.txt
      321  04-06-2017 21:37   routes.txt
     3664  04-06-2017 21:37   stops.txt
   161621  04-06-2017 21:37   stop_times.txt
     3323  04-06-2017 21:37   trips.txt
     9826  04-06-2017 21:37   shapes.txt
      696  04-06-2017 21:37   transfers.txt
      122  04-06-2017 21:37   transfers2.txt
---------                     -------
   179826                     9 files 
```
## [bigbluebus.zip](https://media.metro.net/GTFS/bigbluebus.zip)     
```bash
 Archive:  bigbluebus.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      126  04-06-2017 22:56   agency.txt
      308  04-06-2017 22:59   calendar.txt
     4956  04-06-2017 22:59   routes.txt
    34327  04-06-2017 22:59   stops.txt
  8118517  04-06-2017 22:59   stop_times.txt
   316102  04-06-2017 22:59   trips.txt
   137017  04-06-2017 22:59   shapes.txt
     3944  04-06-2017 22:59   transfers.txt
     1326  04-06-2017 22:59   transfers2.txt
---------                     -------
  8616623                     9 files 
```
## [bolt_bus.zip](https://media.metro.net/GTFS/bolt_bus.zip)     
```bash
 Archive:  bolt_bus.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      119  04-06-2017 21:38   agency.txt
      275  04-06-2017 21:39   calendar.txt
      181  04-06-2017 21:39   routes.txt
      283  04-06-2017 21:39   stops.txt
     1366  04-06-2017 21:39   stop_times.txt
      716  04-06-2017 21:39   trips.txt
      227  04-06-2017 21:39   shapes.txt
       38  04-06-2017 21:39   transfers.txt
       38  04-06-2017 21:39   transfers2.txt
---------                     -------
     3243                     9 files 
```
## [burbank_bus.zip](https://media.metro.net/GTFS/burbank_bus.zip)     
```bash
 Archive:  burbank_bus.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      125  04-06-2017 21:40   agency.txt
      126  04-06-2017 21:41   calendar.txt
      293  04-06-2017 21:41   routes.txt
     5533  04-06-2017 21:41   stops.txt
   302695  04-06-2017 21:41   stop_times.txt
    13327  04-06-2017 21:41   trips.txt
     7532  04-06-2017 21:41   shapes.txt
      542  04-06-2017 21:41   transfers.txt
       38  04-06-2017 21:41   transfers2.txt
---------                     -------
   330211                     9 files 
```
## [calabasas_trolley.zip](https://media.metro.net/GTFS/calabasas_trolley.zip)     
```bash
 Archive:  calabasas_trolley.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      144  04-06-2017 21:52   agency.txt
      263  04-06-2017 21:53   calendar.txt
      140  04-06-2017 21:53   routes.txt
     3681  04-06-2017 21:53   stops.txt
    65069  04-06-2017 21:53   stop_times.txt
     2412  04-06-2017 21:53   trips.txt
     5602  04-06-2017 21:53   shapes.txt
      178  04-06-2017 21:53   transfers.txt
       66  04-06-2017 21:53   transfers2.txt
---------                     -------
    77555                     9 files 
```
## [camarillo_area.zip](https://media.metro.net/GTFS/camarillo_area.zip)     
```bash
 Archive:  camarillo_area.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      140  04-06-2017 21:47   agency.txt
      126  04-06-2017 21:49   calendar.txt
      385  04-06-2017 21:49   routes.txt
     1142  04-06-2017 21:49   stops.txt
    12636  04-06-2017 21:49   stop_times.txt
     1076  04-06-2017 21:49   trips.txt
     9278  04-06-2017 21:49   shapes.txt
       38  04-06-2017 21:49   transfers.txt
       38  04-06-2017 21:49   transfers2.txt
---------                     -------
    24859                     9 files 
```
## [carson_circuit.zip](https://media.metro.net/GTFS/carson_circuit.zip)     
```bash
 Archive:  carson_circuit.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      141  04-06-2017 21:41   agency.txt
      199  04-06-2017 21:42   calendar.txt
     1074  04-06-2017 21:42   routes.txt
    11872  04-06-2017 21:42   stops.txt
   756398  04-06-2017 21:42   stop_times.txt
    18665  04-06-2017 21:42   trips.txt
    61404  04-06-2017 21:42   shapes.txt
     1186  04-06-2017 21:42   transfers.txt
       66  04-06-2017 21:42   transfers2.txt
---------                     -------
   851005                     9 files 
```
## [catalina_express.zip](https://media.metro.net/GTFS/catalina_express.zip)     
```bash
 Archive:  catalina_express.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      135  04-06-2017 21:44   agency.txt
      443  04-06-2017 21:45   calendar.txt
      577  04-06-2017 21:45   routes.txt
      462  04-06-2017 21:45   stops.txt
     8942  04-06-2017 21:45   stop_times.txt
     4350  04-06-2017 21:45   trips.txt
      840  04-06-2017 21:45   shapes.txt
       80  04-06-2017 21:45   transfers.txt
       38  04-06-2017 21:45   transfers2.txt
---------                     -------
    15867                     9 files 
```
## [cerritos_wheels.zip](https://media.metro.net/GTFS/cerritos_wheels.zip)     
```bash
 Archive:  cerritos_wheels.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      162  04-06-2017 21:53   agency.txt
      165  04-06-2017 21:55   calendar.txt
      203  04-06-2017 21:55   routes.txt
     5452  04-06-2017 21:55   stops.txt
   154793  04-06-2017 21:55   stop_times.txt
     2113  04-06-2017 21:55   trips.txt
    10261  04-06-2017 21:55   shapes.txt
      598  04-06-2017 21:55   transfers.txt
       94  04-06-2017 21:55   transfers2.txt
---------                     -------
   173841                     9 files 
```
## [city_commerce.zip](https://media.metro.net/GTFS/city_commerce.zip)     
```bash
 Archive:  city_commerce.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      153  04-06-2017 21:49   agency.txt
      239  04-06-2017 21:50   calendar.txt
     1914  04-06-2017 21:50   routes.txt
     8884  04-06-2017 21:50   stops.txt
   361466  04-06-2017 21:50   stop_times.txt
     9996  04-06-2017 21:50   trips.txt
   101138  04-06-2017 21:50   shapes.txt
     1340  04-06-2017 21:50   transfers.txt
      206  04-06-2017 21:50   transfers2.txt
---------                     -------
   485336                     9 files 
```
## [commuter_express.zip](https://media.metro.net/GTFS/commuter_express.zip)     
```bash
 Archive:  commuter_express.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      150  04-06-2017 21:55   agency.txt
      166  04-06-2017 21:56   calendar.txt
     3103  04-06-2017 21:56   routes.txt
    27860  04-06-2017 21:56   stops.txt
   605423  04-06-2017 21:56   stop_times.txt
    25599  04-06-2017 21:56   trips.txt
    79188  04-06-2017 21:56   shapes.txt
     5596  04-06-2017 21:56   transfers.txt
     2138  04-06-2017 21:56   transfers2.txt
---------                     -------
   749223                     9 files 
```
## [compton_renaissance.zip](https://media.metro.net/GTFS/compton_renaissance.zip)     
```bash
 Archive:  compton_renaissance.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      134  04-06-2017 21:51   agency.txt
      199  04-06-2017 21:52   calendar.txt
     1060  04-06-2017 21:52   routes.txt
     9275  04-06-2017 21:52   stops.txt
   513964  04-06-2017 21:52   stop_times.txt
    10144  04-06-2017 21:52   trips.txt
    47406  04-06-2017 21:52   shapes.txt
     1368  04-06-2017 21:52   transfers.txt
      122  04-06-2017 21:52   transfers2.txt
---------                     -------
   583672                     9 files 
```
## [corona_cruiser.zip](https://media.metro.net/GTFS/corona_cruiser.zip)     
```bash
 Archive:  corona_cruiser.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      129  04-06-2017 21:46   agency.txt
      199  04-06-2017 21:47   calendar.txt
      755  04-06-2017 21:47   routes.txt
     8400  04-06-2017 21:47   stops.txt
   203333  04-06-2017 21:47   stop_times.txt
     5847  04-06-2017 21:47   trips.txt
    21311  04-06-2017 21:47   shapes.txt
      360  04-06-2017 21:47   transfers.txt
      150  04-06-2017 21:47   transfers2.txt
---------                     -------
   240484                     9 files 
```
## [cudahy_cart.zip](https://media.metro.net/GTFS/cudahy_cart.zip)     
```bash
 Archive:  cudahy_cart.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      124  04-06-2017 21:45   agency.txt
      199  04-06-2017 21:46   calendar.txt
      149  04-06-2017 21:46   routes.txt
     2148  04-06-2017 21:46   stops.txt
    38764  04-06-2017 21:46   stop_times.txt
     1143  04-06-2017 21:46   trips.txt
     3713  04-06-2017 21:46   shapes.txt
      164  04-06-2017 21:46   transfers.txt
       38  04-06-2017 21:46   transfers2.txt
---------                     -------
    46442                     9 files 
```
## [culver_city.zip](https://media.metro.net/GTFS/culver_city.zip)     
```bash
 Archive:  culver_city.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      130  04-06-2017 21:42   agency.txt
      339  04-06-2017 21:44   calendar.txt
     1720  04-06-2017 21:44   routes.txt
    15846  04-06-2017 21:44   stops.txt
  3341858  04-06-2017 21:44   stop_times.txt
   108351  04-06-2017 21:44   trips.txt
    55067  04-06-2017 21:44   shapes.txt
     1914  04-06-2017 21:44   transfers.txt
      598  04-06-2017 21:44   transfers2.txt
---------                     -------
  3525823                     9 files 
```
## [dash.zip](https://media.metro.net/GTFS/dash.zip)     
```bash
 Archive:  dash.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      136  04-06-2017 21:56   agency.txt
      347  04-06-2017 21:59   calendar.txt
     5272  04-06-2017 21:59   routes.txt
    76543  04-06-2017 21:59   stops.txt
  9973100  04-06-2017 21:59   stop_times.txt
   319078  04-06-2017 21:59   trips.txt
   224330  04-06-2017 21:59   shapes.txt
    10790  04-06-2017 21:59   transfers.txt
     3230  04-06-2017 21:59   transfers2.txt
---------                     -------
 10612826                     9 files 
```
## [downeylink.zip](https://media.metro.net/GTFS/downeylink.zip)     
```bash
 Archive:  downeylink.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      187  04-06-2017 21:59   agency.txt
      126  04-06-2017 22:00   calendar.txt
      423  04-06-2017 22:00   routes.txt
     5825  04-06-2017 22:00   stops.txt
   122877  04-06-2017 22:00   stop_times.txt
     5202  04-06-2017 22:00   trips.txt
     9235  04-06-2017 22:00   shapes.txt
      500  04-06-2017 22:00   transfers.txt
       38  04-06-2017 22:00   transfers2.txt
---------                     -------
   144413                     9 files 
```
## [duarte_transit.zip](https://media.metro.net/GTFS/duarte_transit.zip)     
```bash
 Archive:  duarte_transit.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      189  04-06-2017 22:00   agency.txt
      199  04-06-2017 22:01   calendar.txt
      726  04-06-2017 22:01   routes.txt
     4989  04-06-2017 22:01   stops.txt
   204063  04-06-2017 22:01   stop_times.txt
     2487  04-06-2017 22:01   trips.txt
    34155  04-06-2017 22:01   shapes.txt
      556  04-06-2017 22:01   transfers.txt
       38  04-06-2017 22:01   transfers2.txt
---------                     -------
   247402                     9 files 
```
## [el_monte.zip](https://media.metro.net/GTFS/el_monte.zip)     
```bash
 Archive:  el_monte.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      187  04-06-2017 22:01   agency.txt
      201  04-06-2017 22:03   calendar.txt
     1098  04-06-2017 22:03   routes.txt
     8804  04-06-2017 22:03   stops.txt
   582005  04-06-2017 22:03   stop_times.txt
    12308  04-06-2017 22:03   trips.txt
    45014  04-06-2017 22:03   shapes.txt
     1032  04-06-2017 22:03   transfers.txt
      178  04-06-2017 22:03   transfers2.txt
---------                     -------
   650827                     9 files 
```
## [el_segundo.zip](https://media.metro.net/GTFS/el_segundo.zip)     
```bash
 Archive:  el_segundo.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      158  04-06-2017 22:03   agency.txt
      126  04-06-2017 22:04   calendar.txt
      215  04-06-2017 22:04   routes.txt
     1078  04-06-2017 22:04   stops.txt
    21150  04-06-2017 22:04   stop_times.txt
     1386  04-06-2017 22:04   trips.txt
     2050  04-06-2017 22:04   shapes.txt
      122  04-06-2017 22:04   transfers.txt
       38  04-06-2017 22:04   transfers2.txt
---------                     -------
    26323                     9 files 
```
## [flyaway.zip](https://media.metro.net/GTFS/flyaway.zip)     
```bash
 Archive:  flyaway.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      116  04-06-2017 22:04   agency.txt
      201  04-06-2017 22:05   calendar.txt
      495  04-06-2017 22:05   routes.txt
     1085  04-06-2017 22:05   stops.txt
   292002  04-06-2017 22:05   stop_times.txt
    31679  04-06-2017 22:05   trips.txt
     7903  04-06-2017 22:05   shapes.txt
      192  04-06-2017 22:05   transfers.txt
       38  04-06-2017 22:05   transfers2.txt
---------                     -------
   333711                     9 files 
```
## [foothill_transit.zip](https://media.metro.net/GTFS/foothill_transit.zip)     
```bash
 Archive:  foothill_transit.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      135  04-06-2017 22:05   agency.txt
      415  04-06-2017 22:08   calendar.txt
     7749  04-06-2017 22:08   routes.txt
    72991  04-06-2017 22:08   stops.txt
 11108607  04-06-2017 22:08   stop_times.txt
   269217  04-06-2017 22:08   trips.txt
   253905  04-06-2017 22:08   shapes.txt
     6520  04-06-2017 22:08   transfers.txt
     1718  04-06-2017 22:08   transfers2.txt
---------                     -------
 11721257                     9 files 
```
## [gardena_municipal.zip](https://media.metro.net/GTFS/gardena_municipal.zip)     
```bash
 Archive:  gardena_municipal.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      185  04-06-2017 22:08   agency.txt
      161  04-06-2017 22:10   calendar.txt
     2918  04-06-2017 22:10   routes.txt
    20877  04-06-2017 22:10   stops.txt
  4420613  04-06-2017 22:10   stop_times.txt
    49464  04-06-2017 22:10   trips.txt
   192599  04-06-2017 22:10   shapes.txt
     2418  04-06-2017 22:10   transfers.txt
      458  04-06-2017 22:10   transfers2.txt
---------                     -------
  4689693                     9 files 
```
## [glendale_beeline.zip](https://media.metro.net/GTFS/glendale_beeline.zip)     
```bash
 Archive:  glendale_beeline.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      136  04-06-2017 22:10   agency.txt
      274  04-06-2017 22:11   calendar.txt
     1840  04-06-2017 22:11   routes.txt
    13958  04-06-2017 22:11   stops.txt
  1285733  04-06-2017 22:11   stop_times.txt
    61356  04-06-2017 22:11   trips.txt
    30352  04-06-2017 22:11   shapes.txt
     1424  04-06-2017 22:11   transfers.txt
      318  04-06-2017 22:11   transfers2.txt
---------                     -------
  1395391                     9 files 
```
## [go_west.zip](https://media.metro.net/GTFS/go_west.zip)     
```bash
 Archive:  go_west.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      137  04-06-2017 23:09   agency.txt
      126  04-06-2017 23:10   calendar.txt
      662  04-06-2017 23:10   routes.txt
     7234  04-06-2017 23:10   stops.txt
   253841  04-06-2017 23:10   stop_times.txt
     5762  04-06-2017 23:10   trips.txt
    28253  04-06-2017 23:10   shapes.txt
      724  04-06-2017 23:10   transfers.txt
       66  04-06-2017 23:10   transfers2.txt
---------                     -------
   296805                     9 files 
```
## [gold_coast.zip](https://media.metro.net/GTFS/gold_coast.zip)     
```bash
 Archive:  gold_coast.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      138  04-06-2017 22:11   agency.txt
      308  04-06-2017 22:13   calendar.txt
     2563  04-06-2017 22:13   routes.txt
    28399  04-06-2017 22:13   stops.txt
  2283689  04-06-2017 22:13   stop_times.txt
    74130  04-06-2017 22:13   trips.txt
    73942  04-06-2017 22:13   shapes.txt
     1270  04-06-2017 22:13   transfers.txt
      150  04-06-2017 22:13   transfers2.txt
---------                     -------
  2464589                     9 files 
```
## [huntington_park.zip](https://media.metro.net/GTFS/huntington_park.zip)     
```bash
 Archive:  huntington_park.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      130  04-06-2017 22:13   agency.txt
      201  04-06-2017 22:14   calendar.txt
      217  04-06-2017 22:14   routes.txt
     3302  04-06-2017 22:14   stops.txt
   232559  04-06-2017 22:14   stop_times.txt
     5379  04-06-2017 22:14   trips.txt
     5019  04-06-2017 22:14   shapes.txt
      262  04-06-2017 22:14   transfers.txt
       38  04-06-2017 22:14   transfers2.txt
---------                     -------
   247107                     9 files 
```
## [inglewood_iline.zip](https://media.metro.net/GTFS/inglewood_iline.zip)     
```bash
 Archive:  inglewood_iline.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      211  04-06-2017 22:14   agency.txt
      126  04-06-2017 22:15   calendar.txt
      179  04-06-2017 22:15   routes.txt
     1659  04-06-2017 22:15   stops.txt
    20256  04-06-2017 22:15   stop_times.txt
      499  04-06-2017 22:15   trips.txt
     7471  04-06-2017 22:15   shapes.txt
      178  04-06-2017 22:15   transfers.txt
       38  04-06-2017 22:15   transfers2.txt
---------                     -------
    30617                     9 files 
```
## [irvine_ishuttle.zip](https://media.metro.net/GTFS/irvine_ishuttle.zip)     
```bash
 Archive:  irvine_ishuttle.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      141  04-06-2017 22:15   agency.txt
      126  04-06-2017 22:16   calendar.txt
      561  04-06-2017 22:16   routes.txt
     3974  04-06-2017 22:16   stops.txt
   153421  04-06-2017 22:16   stop_times.txt
    11413  04-06-2017 22:16   trips.txt
     6332  04-06-2017 22:16   shapes.txt
      528  04-06-2017 22:16   transfers.txt
       94  04-06-2017 22:16   transfers2.txt
---------                     -------
   176590                     9 files 
```
## [kanan_shuttle.zip](https://media.metro.net/GTFS/kanan_shuttle.zip)     
```bash
 Archive:  kanan_shuttle.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      129  04-06-2017 22:16   agency.txt
      165  04-06-2017 22:17   calendar.txt
       95  04-06-2017 22:17   routes.txt
      826  04-06-2017 22:17   stops.txt
    27005  04-06-2017 22:17   stop_times.txt
     1576  04-06-2017 22:17   trips.txt
     1187  04-06-2017 22:17   shapes.txt
       52  04-06-2017 22:17   transfers.txt
       38  04-06-2017 22:17   transfers2.txt
---------                     -------
    31073                     9 files 
```
## [la_county.zip](https://media.metro.net/GTFS/la_county.zip)     
```bash
 Archive:  la_county.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      145  04-06-2017 21:50   agency.txt
      416  04-06-2017 21:51   calendar.txt
     3331  04-06-2017 21:51   routes.txt
    31128  04-06-2017 21:51   stops.txt
  1367996  04-06-2017 21:51   stop_times.txt
    41754  04-06-2017 21:51   trips.txt
   105029  04-06-2017 21:51   shapes.txt
     3398  04-06-2017 21:51   transfers.txt
      430  04-06-2017 21:51   transfers2.txt
---------                     -------
  1553627                     9 files 
```
## [la_habra.zip](https://media.metro.net/GTFS/la_habra.zip)     
```bash
 Archive:  la_habra.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      132  04-06-2017 22:21   agency.txt
      126  04-06-2017 22:22   calendar.txt
      389  04-06-2017 22:22   routes.txt
     1246  04-06-2017 22:22   stops.txt
    29711  04-06-2017 22:22   stop_times.txt
     2294  04-06-2017 22:22   trips.txt
     4539  04-06-2017 22:22   shapes.txt
      122  04-06-2017 22:22   transfers.txt
       38  04-06-2017 22:22   transfers2.txt
---------                     -------
    38597                     9 files 
```
## [la_puente.zip](https://media.metro.net/GTFS/la_puente.zip)     
```bash
 Archive:  la_puente.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      156  04-06-2017 22:22   agency.txt
      239  04-06-2017 22:23   calendar.txt
      337  04-06-2017 22:23   routes.txt
     2779  04-06-2017 22:23   stops.txt
    96974  04-06-2017 22:23   stop_times.txt
     2301  04-06-2017 22:23   trips.txt
    10929  04-06-2017 22:23   shapes.txt
      248  04-06-2017 22:23   transfers.txt
       38  04-06-2017 22:23   transfers2.txt
---------                     -------
   114001                     9 files 
```
## [lawa.zip](https://media.metro.net/GTFS/lawa.zip)     
```bash
 Archive:  lawa.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      158  04-06-2017 21:30   agency.txt
      201  04-06-2017 21:32   calendar.txt
      259  04-06-2017 21:32   routes.txt
      755  04-06-2017 21:32   stops.txt
   311565  04-06-2017 21:32   stop_times.txt
    27202  04-06-2017 21:32   trips.txt
     4385  04-06-2017 21:32   shapes.txt
      178  04-06-2017 21:32   transfers.txt
       38  04-06-2017 21:32   transfers2.txt
---------                     -------
   344741                     9 files 
```
## [lawndale_trolley.zip](https://media.metro.net/GTFS/lawndale_trolley.zip)     
```bash
 Archive:  lawndale_trolley.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      159  04-06-2017 22:23   agency.txt
      166  04-06-2017 22:25   calendar.txt
      205  04-06-2017 22:25   routes.txt
     1999  04-06-2017 22:25   stops.txt
    53304  04-06-2017 22:25   stop_times.txt
     2555  04-06-2017 22:25   trips.txt
     2989  04-06-2017 22:25   shapes.txt
      276  04-06-2017 22:25   transfers.txt
       38  04-06-2017 22:25   transfers2.txt
---------                     -------
    61691                     9 files 
```
## [long_beach.zip](https://media.metro.net/GTFS/long_beach.zip)     
```bash
 Archive:  long_beach.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      132  04-06-2017 22:17   agency.txt
      347  04-06-2017 22:21   calendar.txt
     8746  04-06-2017 22:21   routes.txt
    65556  04-06-2017 22:21   stops.txt
 16545515  04-06-2017 22:21   stop_times.txt
   392635  04-06-2017 22:21   trips.txt
   307467  04-06-2017 22:21   shapes.txt
     6464  04-06-2017 22:21   transfers.txt
     2390  04-06-2017 22:21   transfers2.txt
---------                     -------
 17329252                     9 files 
```
## [lynwood_trolley.zip](https://media.metro.net/GTFS/lynwood_trolley.zip)     
```bash
 Archive:  lynwood_trolley.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      145  04-06-2017 22:25   agency.txt
      201  04-06-2017 22:26   calendar.txt
      426  04-06-2017 22:26   routes.txt
     5643  04-06-2017 22:26   stops.txt
   289344  04-06-2017 22:26   stop_times.txt
     6728  04-06-2017 22:26   trips.txt
    25980  04-06-2017 22:26   shapes.txt
      640  04-06-2017 22:26   transfers.txt
       66  04-06-2017 22:26   transfers2.txt
---------                     -------
   329173                     9 files 
```
## [megabus.zip](https://media.metro.net/GTFS/megabus.zip)     
```bash
 Archive:  megabus.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      118  04-06-2017 22:28   agency.txt
      312  04-06-2017 22:29   calendar.txt
      181  04-06-2017 22:29   routes.txt
      240  04-06-2017 22:29   stops.txt
     2000  04-06-2017 22:29   stop_times.txt
     1048  04-06-2017 22:29   trips.txt
      227  04-06-2017 22:29   shapes.txt
       52  04-06-2017 22:29   transfers.txt
       38  04-06-2017 22:29   transfers2.txt
---------                     -------
     4216                     9 files 
```
## [metro_bus.zip](https://media.metro.net/GTFS/metro_bus.zip)     
```bash
 Archive:  metro_bus.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      163  12-01-2014 16:37   agency.txt
     3896  11-15-2016 11:29   calendar.txt
     9861  03-24-2017 10:43   calendar_dates.txt
      282  03-24-2017 16:29   feed_info.txt
     6557  03-24-2017 10:42   routes.txt
 33731737  03-24-2017 10:45   shapes.txt
138935886  03-24-2017 12:16   stop_times.txt
   817084  03-24-2017 12:14   stops.txt
  2258870  03-24-2017 12:17   trips.txt
---------                     -------
175764336                     9 files 
```
## [metro_rail.zip](https://media.metro.net/GTFS/metro_rail.zip)     
```bash
 Archive:  metro_rail.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      173  04-07-2017 00:08   agency.txt
     1531  04-07-2017 00:08   calendar.txt
     1175  04-07-2017 00:08   calendar_dates.txt
      642  04-07-2017 00:08   routes.txt
   273495  03-16-2016 16:09   shapes.txt
    33465  02-08-2017 15:08   stops.txt
 11399166  04-07-2017 00:09   stop_times.txt
   511314  04-07-2017 00:08   trips.txt
---------                     -------
 12220961                     8 files 
```
## [metrolink.zip](https://media.metro.net/GTFS/metrolink.zip)     
```bash
 Archive:  metrolink.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      129  04-06-2017 22:29   agency.txt
      229  04-06-2017 22:30   calendar.txt
     3889  04-06-2017 22:30   routes.txt
     4729  04-06-2017 22:30   stops.txt
   192875  04-06-2017 22:30   stop_times.txt
    19097  04-06-2017 22:30   trips.txt
    20175  04-06-2017 22:30   shapes.txt
      556  04-06-2017 22:30   transfers.txt
       38  04-06-2017 22:30   transfers2.txt
---------                     -------
   241717                     9 files 
```
## [monrovia_trolley.zip](https://media.metro.net/GTFS/monrovia_trolley.zip)     
```bash
 Archive:  monrovia_trolley.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      135  04-06-2017 22:32   agency.txt
      126  04-06-2017 22:33   calendar.txt
       98  04-06-2017 22:33   routes.txt
      629  04-06-2017 22:33   stops.txt
    28630  04-06-2017 22:33   stop_times.txt
     2487  04-06-2017 22:33   trips.txt
      919  04-06-2017 22:33   shapes.txt
       66  04-06-2017 22:33   transfers.txt
       38  04-06-2017 22:33   transfers2.txt
---------                     -------
    33128                     9 files 
```
## [montebello_bus.zip](https://media.metro.net/GTFS/montebello_bus.zip)     
```bash
 Archive:  montebello_bus.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      166  04-06-2017 22:26   agency.txt
      229  04-06-2017 22:28   calendar.txt
     3097  04-06-2017 22:28   routes.txt
    29790  04-06-2017 22:28   stops.txt
  5453425  04-06-2017 22:28   stop_times.txt
   113644  04-06-2017 22:28   trips.txt
    91158  04-06-2017 22:28   shapes.txt
     3286  04-06-2017 22:28   transfers.txt
      878  04-06-2017 22:28   transfers2.txt
---------                     -------
  5695673                     9 files 
```
## [monterey_park.zip](https://media.metro.net/GTFS/monterey_park.zip)     
```bash
 Archive:  monterey_park.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      179  04-06-2017 22:30   agency.txt
      199  04-06-2017 22:31   calendar.txt
      819  04-06-2017 22:31   routes.txt
     6307  04-06-2017 22:31   stops.txt
   255911  04-06-2017 22:31   stop_times.txt
     9643  04-06-2017 22:31   trips.txt
    24443  04-06-2017 22:31   shapes.txt
      542  04-06-2017 22:31   transfers.txt
       94  04-06-2017 22:31   transfers2.txt
---------                     -------
   298137                     9 files 
```
## [moorpark_transit.zip](https://media.metro.net/GTFS/moorpark_transit.zip)     
```bash
 Archive:  moorpark_transit.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      149  04-06-2017 22:31   agency.txt
      160  04-06-2017 22:32   calendar.txt
      385  04-06-2017 22:32   routes.txt
     4407  04-06-2017 22:32   stops.txt
    89311  04-06-2017 22:32   stop_times.txt
     2724  04-06-2017 22:32   trips.txt
    10024  04-06-2017 22:32   shapes.txt
      234  04-06-2017 22:32   transfers.txt
       38  04-06-2017 22:32   transfers2.txt
---------                     -------
   107432                     9 files 
```
## [norwalk_transit.zip](https://media.metro.net/GTFS/norwalk_transit.zip)     
```bash
 Archive:  norwalk_transit.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      150  04-06-2017 22:33   agency.txt
      195  04-06-2017 22:35   calendar.txt
     2681  04-06-2017 22:35   routes.txt
    18766  04-06-2017 22:35   stops.txt
  1629309  04-06-2017 22:35   stop_times.txt
    39269  04-06-2017 22:35   trips.txt
    74756  04-06-2017 22:35   shapes.txt
     1844  04-06-2017 22:35   transfers.txt
      178  04-06-2017 22:35   transfers2.txt
---------                     -------
  1767148                     9 files 
```
## [ojai_trolley.zip](https://media.metro.net/GTFS/ojai_trolley.zip)     
```bash
 Archive:  ojai_trolley.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      126  04-06-2017 22:44   agency.txt
      240  04-06-2017 22:45   calendar.txt
      217  04-06-2017 22:45   routes.txt
     2661  04-06-2017 22:45   stops.txt
   108630  04-06-2017 22:45   stop_times.txt
     2837  04-06-2017 22:45   trips.txt
     7506  04-06-2017 22:45   shapes.txt
      122  04-06-2017 22:45   transfers.txt
       38  04-06-2017 22:45   transfers2.txt
---------                     -------
   122377                     9 files 
```
## [omnitrans.zip](https://media.metro.net/GTFS/omnitrans.zip)     
```bash
 Archive:  omnitrans.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      123  04-06-2017 22:41   agency.txt
      268  04-06-2017 22:44   calendar.txt
     7235  04-06-2017 22:44   routes.txt
    93525  04-06-2017 22:44   stops.txt
 13663845  04-06-2017 22:44   stop_times.txt
   309731  04-06-2017 22:44   trips.txt
   233912  04-06-2017 22:44   shapes.txt
     3440  04-06-2017 22:44   transfers.txt
      990  04-06-2017 22:44   transfers2.txt
---------                     -------
 14313069                     9 files 
```
## [orange_county.zip](https://media.metro.net/GTFS/orange_county.zip)     
```bash
 Archive:  orange_county.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      147  04-06-2017 22:35   agency.txt
      375  04-06-2017 22:41   calendar.txt
    17508  04-06-2017 22:41   routes.txt
   209654  04-06-2017 22:41   stops.txt
 31044181  04-06-2017 22:41   stop_times.txt
   641079  04-06-2017 22:41   trips.txt
   643798  04-06-2017 22:41   shapes.txt
    13212  04-06-2017 22:41   transfers.txt
     2810  04-06-2017 22:41   transfers2.txt
---------                     -------
 32572764                     9 files 
```
## [palos_verdes.zip](https://media.metro.net/GTFS/palos_verdes.zip)     
```bash
 Archive:  palos_verdes.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      167  04-06-2017 22:47   agency.txt
      126  04-06-2017 22:48   calendar.txt
      393  04-06-2017 22:48   routes.txt
     6462  04-06-2017 22:48   stops.txt
   104465  04-06-2017 22:48   stop_times.txt
     1708  04-06-2017 22:48   trips.txt
    19924  04-06-2017 22:48   shapes.txt
      332  04-06-2017 22:48   transfers.txt
       94  04-06-2017 22:48   transfers2.txt
---------                     -------
   133671                     9 files 
```
## [paramount_easy.zip](https://media.metro.net/GTFS/paramount_easy.zip)     
```bash
 Archive:  paramount_easy.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      146  04-06-2017 22:48   agency.txt
      160  04-06-2017 22:49   calendar.txt
      389  04-06-2017 22:49   routes.txt
     3238  04-06-2017 22:49   stops.txt
   120000  04-06-2017 22:49   stop_times.txt
     3543  04-06-2017 22:49   trips.txt
     8955  04-06-2017 22:49   shapes.txt
      430  04-06-2017 22:49   transfers.txt
       94  04-06-2017 22:49   transfers2.txt
---------                     -------
   136955                     9 files 
```
## [pasadena_transit.zip](https://media.metro.net/GTFS/pasadena_transit.zip)     
```bash
 Archive:  pasadena_transit.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      172  04-06-2017 22:45   agency.txt
      199  04-06-2017 22:47   calendar.txt
     2000  04-06-2017 22:47   routes.txt
    16008  04-06-2017 22:47   stops.txt
  2075503  04-06-2017 22:47   stop_times.txt
    56852  04-06-2017 22:47   trips.txt
    62259  04-06-2017 22:47   shapes.txt
     2250  04-06-2017 22:47   transfers.txt
      626  04-06-2017 22:47   transfers2.txt
---------                     -------
  2215869                     9 files 
```
## [riverside_transit.zip](https://media.metro.net/GTFS/riverside_transit.zip)     
```bash
 Archive:  riverside_transit.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      144  04-06-2017 22:49   agency.txt
      342  04-06-2017 22:52   calendar.txt
    12573  04-06-2017 22:52   routes.txt
   109366  04-06-2017 22:52   stops.txt
 10218213  04-06-2017 22:52   stop_times.txt
   234366  04-06-2017 22:52   trips.txt
   442996  04-06-2017 22:52   shapes.txt
     4406  04-06-2017 22:52   transfers.txt
     1018  04-06-2017 22:52   transfers2.txt
---------                     -------
 11023424                     9 files 
```
## [rosemead_explorer.zip](https://media.metro.net/GTFS/rosemead_explorer.zip)     
```bash
 Archive:  rosemead_explorer.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      157  04-06-2017 22:52   agency.txt
      201  04-06-2017 22:53   calendar.txt
      608  04-06-2017 22:53   routes.txt
     3586  04-06-2017 22:53   stops.txt
   188145  04-06-2017 22:53   stop_times.txt
     3516  04-06-2017 22:53   trips.txt
    23375  04-06-2017 22:53   shapes.txt
      598  04-06-2017 22:53   transfers.txt
       66  04-06-2017 22:53   transfers2.txt
---------                     -------
   220252                     9 files 
```
## [santa_clarita.zip](https://media.metro.net/GTFS/santa_clarita.zip)     
```bash
 Archive:  santa_clarita.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      138  04-06-2017 22:53   agency.txt
      269  04-06-2017 22:55   calendar.txt
     6897  04-06-2017 22:55   routes.txt
    24922  04-06-2017 22:55   stops.txt
  2667172  04-06-2017 22:55   stop_times.txt
    77106  04-06-2017 22:55   trips.txt
   270859  04-06-2017 22:55   shapes.txt
     2236  04-06-2017 22:55   transfers.txt
      822  04-06-2017 22:55   transfers2.txt
---------                     -------
  3050421                     9 files 
```
## [sierra_madre.zip](https://media.metro.net/GTFS/sierra_madre.zip)     
```bash
 Archive:  sierra_madre.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      155  04-06-2017 22:55   agency.txt
      126  04-06-2017 22:56   calendar.txt
      274  04-06-2017 22:56   routes.txt
     2089  04-06-2017 22:56   stops.txt
     9506  04-06-2017 22:56   stop_times.txt
      722  04-06-2017 22:56   trips.txt
     2211  04-06-2017 22:56   shapes.txt
      122  04-06-2017 22:56   transfers.txt
       38  04-06-2017 22:56   transfers2.txt
---------                     -------
    15243                     9 files 
```
## [simi_valley.zip](https://media.metro.net/GTFS/simi_valley.zip)     
```bash
 Archive:  simi_valley.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      158  04-06-2017 23:02   agency.txt
      271  04-06-2017 23:03   calendar.txt
      949  04-06-2017 23:03   routes.txt
     7965  04-06-2017 23:03   stops.txt
   312247  04-06-2017 23:03   stop_times.txt
     9537  04-06-2017 23:03   trips.txt
    20610  04-06-2017 23:03   shapes.txt
      458  04-06-2017 23:03   transfers.txt
       94  04-06-2017 23:03   transfers2.txt
---------                     -------
   352289                     9 files 
```
## [south_gate.zip](https://media.metro.net/GTFS/south_gate.zip)     
```bash
 Archive:  south_gate.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      134  04-06-2017 22:59   agency.txt
      165  04-06-2017 23:00   calendar.txt
      118  04-06-2017 23:00   routes.txt
      834  04-06-2017 23:00   stops.txt
    34747  04-06-2017 23:00   stop_times.txt
     2894  04-06-2017 23:00   trips.txt
      791  04-06-2017 23:00   shapes.txt
      122  04-06-2017 23:00   transfers.txt
       38  04-06-2017 23:00   transfers2.txt
---------                     -------
    39843                     9 files 
```
## [south_whittier.zip](https://media.metro.net/GTFS/south_whittier.zip)     
```bash
 Archive:  south_whittier.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      174  04-06-2017 23:03   agency.txt
      165  04-06-2017 23:04   calendar.txt
      277  04-06-2017 23:04   routes.txt
     6203  04-06-2017 23:04   stops.txt
   181599  04-06-2017 23:04   stop_times.txt
     5215  04-06-2017 23:04   trips.txt
     8371  04-06-2017 23:04   shapes.txt
      416  04-06-2017 23:04   transfers.txt
       94  04-06-2017 23:04   transfers2.txt
---------                     -------
   202514                     9 files 
```
## [sunLine_transit.zip](https://media.metro.net/GTFS/sunLine_transit.zip)     
```bash
 Archive:  sunLine_transit.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      134  04-06-2017 23:00   agency.txt
      274  04-06-2017 23:02   calendar.txt
     2379  04-06-2017 23:02   routes.txt
    28341  04-06-2017 23:02   stops.txt
  2714911  04-06-2017 23:02   stop_times.txt
    78965  04-06-2017 23:02   trips.txt
    56350  04-06-2017 23:02   shapes.txt
     1046  04-06-2017 23:02   transfers.txt
      178  04-06-2017 23:02   transfers2.txt
---------                     -------
  2882578                     9 files 
```
## [thousand_oaks.zip](https://media.metro.net/GTFS/thousand_oaks.zip)     
```bash
 Archive:  thousand_oaks.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      186  04-06-2017 23:04   agency.txt
      199  04-06-2017 23:05   calendar.txt
      916  04-06-2017 23:05   routes.txt
     6138  04-06-2017 23:05   stops.txt
   131163  04-06-2017 23:05   stop_times.txt
     6067  04-06-2017 23:05   trips.txt
    17285  04-06-2017 23:05   shapes.txt
      248  04-06-2017 23:05   transfers.txt
       38  04-06-2017 23:05   transfers2.txt
---------                     -------
   162240                     9 files 
```
## [torrance_transit.zip](https://media.metro.net/GTFS/torrance_transit.zip)     
```bash
 Archive:  torrance_transit.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      131  04-06-2017 23:05   agency.txt
      307  04-06-2017 23:07   calendar.txt
     1580  04-06-2017 23:07   routes.txt
    26488  04-06-2017 23:07   stops.txt
  3485648  04-06-2017 23:07   stop_times.txt
    77428  04-06-2017 23:07   trips.txt
    49750  04-06-2017 23:07   shapes.txt
     3034  04-06-2017 23:07   transfers.txt
      402  04-06-2017 23:07   transfers2.txt
---------                     -------
  3644768                     9 files 
```
## [valley_express.zip](https://media.metro.net/GTFS/valley_express.zip)     
```bash
 Archive:  valley_express.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      152  04-06-2017 23:07   agency.txt
      201  04-06-2017 23:08   calendar.txt
      898  04-06-2017 23:08   routes.txt
     4871  04-06-2017 23:08   stops.txt
   178596  04-06-2017 23:08   stop_times.txt
     9191  04-06-2017 23:08   trips.txt
    12858  04-06-2017 23:08   shapes.txt
       38  04-06-2017 23:08   transfers.txt
       38  04-06-2017 23:08   transfers2.txt
---------                     -------
   206843                     9 files 
```
## [ventura_vista.zip](https://media.metro.net/GTFS/ventura_vista.zip)     
```bash
 Archive:  ventura_vista.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      124  04-06-2017 23:08   agency.txt
      269  04-06-2017 23:09   calendar.txt
     9215  04-06-2017 23:09   routes.txt
     7460  04-06-2017 23:09   stops.txt
   218037  04-06-2017 23:09   stop_times.txt
    37535  04-06-2017 23:09   trips.txt
   241465  04-06-2017 23:09   shapes.txt
      990  04-06-2017 23:09   transfers.txt
      374  04-06-2017 23:09   transfers2.txt
---------                     -------
   515469                     9 files 
```
## [west_hollywood.zip](https://media.metro.net/GTFS/west_hollywood.zip)     
```bash
 Archive:  west_hollywood.zip
  Length      Date    Time    Name
---------  ---------- -----   ----
      183  04-06-2017 23:10   agency.txt
      165  04-06-2017 23:12   calendar.txt
      455  04-06-2017 23:12   routes.txt
     4083  04-06-2017 23:12   stops.txt
   122948  04-06-2017 23:12   stop_times.txt
     5439  04-06-2017 23:12   trips.txt
     6494  04-06-2017 23:12   shapes.txt
      416  04-06-2017 23:12   transfers.txt
      178  04-06-2017 23:12   transfers2.txt
---------                     -------
   140361                     9 files 
```
