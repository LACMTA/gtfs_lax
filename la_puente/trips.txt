route_id,service_id,trip_id,trip_headsign,shape_id
2606.1,S,2606.1-18-0-667-1700-1752-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.1
2606.1,MTWHF,2606.1-19-0-667-1800-1852-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.1
2606.2,MTWHFSU,2606.2-10-0-722-0900-1000-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2606.2,MTWHFSU,2606.2-11-0-722-1000-1100-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2606.2,MTWHFSU,2606.2-12-0-722-1100-1200-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2606.2,MTWHFSU,2606.2-13-0-722-1200-1300-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2606.2,MTWHFSU,2606.2-14-0-722-1300-1400-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2606.2,MTWHFSU,2606.2-15-0-722-1400-1500-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2606.2,MTWHFSU,2606.2-16-0-722-1500-1600-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2606.2,MTWHFSU,2606.2-17-0-722-1600-1700-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2606.2,MTWHF,2606.2-18-0-722-1700-1800-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2606.2,MTWHF,2606.2-7-0-722-0600-0700-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2606.2,MTWHF,2606.2-8-0-722-0700-0800-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2606.2,MTWHF,2606.2-9-0-722-0800-0900-2188-0-0-0-0_LL_5LB,""GREEN ROUTE"",2606.2
2607.1,MTWHF,2607.1-19-0-685-1800-1854-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.1
2607.2,MTWHFSU,2607.2-10-0-760-0900-1000-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
2607.2,MTWHFSU,2607.2-11-0-760-1000-1100-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
2607.2,MTWHFSU,2607.2-12-0-760-1100-1200-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
2607.2,MTWHFSU,2607.2-13-0-760-1200-1300-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
2607.2,MTWHFSU,2607.2-14-0-760-1300-1400-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
2607.2,MTWHFSU,2607.2-15-0-760-1400-1500-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
2607.2,MTWHFSU,2607.2-16-0-760-1500-1600-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
2607.2,MTWHFSU,2607.2-17-0-760-1600-1700-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
2607.2,MTWHFS,2607.2-18-0-760-1700-1800-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
2607.2,MTWHF,2607.2-7-0-760-0600-0700-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
2607.2,MTWHF,2607.2-8-0-760-0700-0800-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
2607.2,MTWHF,2607.2-9-0-760-0800-0900-2258-0-0-0-0_LL_5LB,""YELLOW ROUTE"",2607.2
